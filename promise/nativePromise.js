var promise = new Promise((resolve, reject) => {
	const activate = true;
	if (activate === true) {
      resolve("I'm successful")
    } else if (activate === false) {
      reject("I'm a failure.")
    } else {
      throw new Error("Oh this is not good.");
    }
});

console.log('native promise', promise);

promise
	.then( 
	   value => console.log('native promise:  then success', value),
	   err => console.log('native promise: then error', err)
  )
	.catch( err => {
		console.log('native promise: catch', err)
	})
	.finally( () => { 
		console.log('native promise: finally')
	});