class FakePromiseClass {
	value = undefined;
	status = 'pending';
	
	constructor( callback) {
		const updatePromise = (value, status) => {
			setTimeout( () => {
				if ( this.status !== 'resolved' ) {
					this.value = value;
					this.status = status;
				}
			}, 0);
		}
		
		const resolve = (data) => {
			const value = FakePromiseClass.resolve(data);
			if (value) updatePromise(value, 'resolved');
		}
		
		const reject = (data) => {
			const value = FakePromiseClass.reject(data);
			updatePromise(value, 'rejected');
		}
		
		callback(resolve, reject);
	}

  then( functionSuccess, functionError ) {
		setTimeout ( () => {
			if (this.status !== 'resolved' && !functionError) return this;
			const callback = this.status === 'resolved' ? functionSuccess : functionError;

			setTimeout(callback(this.value), 0);
		}, 0);
		
		return this;
	}

  catch(functionError) {
		setTimeout ( () => {
			if (this.status === 'rejected') functionError(this.value);
		}, 0);
		
		return this;
	}

  finally(functionFinally) {
		setTimeout ( () => {
			functionFinally();
		}, 0);
	}

  static resolve (fakePromiseDataProducer){
		return fakePromiseDataProducer;
	}

  static reject(fakePromiseDataProducer) {
		return fakePromiseDataProducer;
	}
}

var fakePromiseClass = new FakePromiseClass ( (resolve, reject) => {
	const activate = false;
	if (activate === true) {
      resolve("I'm successful")
    } else if (activate === false) {
      reject("I'm a failure.")
    } else {
      throw new Error("Oh this is not good.");
    }
})

fakePromiseClass.then( 
	   value => console.log('fake class: then success', value),
	   err => console.log('fake class: then error', err)
  )
	.catch( err => {
		console.log('fake class: cathc', err)
	})
	.finally( () => { 
		console.log('fake class: finally')
	});