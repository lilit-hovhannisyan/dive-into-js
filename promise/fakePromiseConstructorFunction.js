function FakePromiseFunction( callback ) {
	this.value = undefined;
	this.status = 'pending';
	
	const updatePromise = (value, status) => {
			setTimeout( () => {
				if ( this.status !== 'resolved' ) {
					this.value = value;
					this.status = status;
				}
			}, 0);
		}
		
	const resolve = (data) => {
		const value = FakePromiseFunction.resolve(data);
		if (value) updatePromise(value, 'resolved');
	}

	const reject = (data) => {
		const value = FakePromiseFunction.reject(data);
		updatePromise(value, 'rejected');
	}
	
	callback(resolve, reject);
}

FakePromiseFunction.prototype = {
	then( functionSuccess, functionError ) {
		setTimeout ( () => {
			if (this.status !== 'resolved' && !functionError) return this;
			const callback = this.status === 'resolved' ? functionSuccess : functionError;

			setTimeout(callback(this.value), 0);
		}, 0);
		
		return this;
	},

  catch(functionError) {
		setTimeout ( () => {
			if (this.status === 'rejected') functionError(this.value);
		}, 0);
		
		return this;
	},

  finally(functionFinally) {
		setTimeout ( () => {
			functionFinally();
		}, 0);
	},
}

FakePromiseFunction.resolve = function(fakePromiseDataProducer){
	return fakePromiseDataProducer;
}

FakePromiseFunction.reject = function(fakePromiseDataProducer) {
	return fakePromiseDataProducer;
}


var fakePromiseFunction = new FakePromiseFunction( (resolve, reject) => {
	const activate = false;
	if (activate === true) {
      resolve("I'm successful");
    } else if (activate === false) {
      reject("I'm a failure.");
    } else {
      throw new Error("Oh this is not good.");
    }
})

console.log(fakePromiseFunction)

fakePromiseFunction
	.then( 
	   value => console.log('fake func: then success', value),
	   err => console.log('fake func: then error', err)
  )
	.catch( err => {
		console.log('fake func: cathc', err)
	})
	.finally( () => { 
		console.log('fake func: finally')
	});